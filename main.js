const shSample = document.getElementById("shSample");
const hSample = document.getElementById("hSampleHide");
const jsDerbSamp = document.getElementById("jsDerbySampHidden");
const jsDerbSampShown = document.getElementById("jsDerbySampShown");
const showSample = document.getElementById("showSample");
const hideSample = document.getElementById("hiSampleHide");
const joustSamp = document.getElementById("joustSampHidden");
const joustSampShown = document.getElementById("joustSampShown");

shSample.onclick = function() {

    jsDerbSamp.removeAttribute("id");
    jsDerbSamp.id = "jsDerbySampShown";

    shSample.removeAttribute("id");
    shSample.id = "shSampleHide";

    hSample.removeAttribute("id");
    hSample.id = "hSample"
}

hSample.onclick = function() {

    jsDerbSamp.removeAttribute("id");
    jsDerbSamp.id = "jsDerbySampHidden";
    joustSamp.scrollIntoView();

    shSample.removeAttribute("id");
    shSample.id = "shSample";

    hSample.removeAttribute("id");
    hSample.id = "hSampleHide";
}


showSample.onclick = function() {

    joustSamp.removeAttribute("id");
    joustSamp.id = "joustSampShown";
    joustSamp.scrollIntoView();

    showSample.removeAttribute("id");
    showSample.id = "showSampleHide";

    hideSample.removeAttribute("id");
    hideSample.id = "hiSample"
}

hideSample.onclick = function() {
    
    joustSamp.removeAttribute("id");
    joustSamp.id = "joustSampHidden";

    showSample.removeAttribute("id");
    showSample.id = "shSample";

    hideSample.removeAttribute("id");
    hideSample.id = "hiSampleHide";
}